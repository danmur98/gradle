package com.epam.jarutils;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringUtilTest {
    @Test
    void testIsPositiveNumber() {
        assertTrue(StringUtils.isPositiveNumber("42"));
        assertFalse(StringUtils.isPositiveNumber("-1"));
        assertFalse(StringUtils.isPositiveNumber("abc"));
    }
}
